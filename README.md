# docker-bigdata

## 介绍

利用 docker 搭建大数据集群

## 软件架构

软件架构说明

## 安装教程

1. xxxx
2. xxxx
3. xxxx

## 使用说明

### Base 包构建

1. alpine 构建命令 `docker build --rm --no-cache -f Dockerfile-alpine -t wuhongbo7/base:alpine-jdk8-py3 .`
2. base 构建命令 `docker build --rm --no-cache -t wuhongbo7/base:centos7-jdk8-py3 .`
3. standalone 构建命令 `docker build --rm --no-cache -t wuhongbo7/alone .`

### 容器运行

1. standalone 运行命令 `docker run -it --hostname namenode --privileged=true --name alone -p 9870:9870 -p 9864:9864 -p 8088:8088 -p 8042:8042 -p 19888:19888 -p 10000:10000 -p 9083:9083 wuhongbo7/alone sh -l`

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

## 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
