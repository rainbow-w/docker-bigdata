#!/bin/bash

set -e

# colors
notice() {
  echo -e "\033[47;30m $1 \033[0m"
}

judge() {
  if [ 0 -eq $? ]; then
    echo -e "\033[47;30m $1 成功 \033[0m"
  else
    echo -e "\033[41;30m $1 失败 \033[0m" && exit 1
  fi
}

downloadPackage() {
  notice "下载安装包"
  # wget -S "${HADOOP_URL}" -O /tmp/hadoop.tar.gz
  # wget -S "${HIVE_URL}" -O /tmp/hive.tar.gz
  curl -ZSso "${HADOOP_URL}" /tmp/hadoop.tar.gz
  curl -ZSso "${HIVE_URL}" /tmp/hive.tar.gz
  tar -xf /tmp/hadoop.tar.gz -C /opt/
  tar -xf /tmp/hive.tar.gz -C /opt/
  rm /tmp/*.tar.gz
  judge "下载"
}

install() {
  notice "开始解压安装"
  mkdir ${HADOOP_HOME}/logs
  python3 /tmp/setConf.py core-site hdfs-site yarn-site mapred-site hive-site
  mkdir -p /home/hadoop/dfs/name
  mkdir -p /home/hadoop/dfs/data
  mkdir -p /home/hadoop/dfs/tmp
  touch /home/hadoop/dfs/dfs.hosts.exclude
  touch /home/hadoop/dfs/dfs.hosts
  rm -rf ${HADOOP_HOME}/bin/*.cmd ${HADOOP_HOME}/sbin/*.cmd ${HADOOP_HOME}/sbin/*all* ${HADOOP_CONF_DIR}/*.cmd
  hdfs namenode -format -y
}

main() {
  wget --spider ${HIVE_URL} ${HADOOP_URL}
  if [[ 0 -eq $? ]]; then
    [[ ! -e ${HADOOP_HOME} ]] && downloadPackage
    install
  else
    HADOOP_SITE=https://hadoop.apache.org/releases.html
    HADOOP_VERSION_LIST=$(wget ${HADOOP_SITE} -q -O - | grep -E '<td>([0-9]+\.[0-9]+\.)' | grep -oE '[0-9\.]+')
    echo "当前可用Hadoop版本号：$(echo $HADOOP_VERSION_LIST | sed 's/\s/, /g') 。"
    HIVE_SITE=https://mirrors.tuna.tsinghua.edu.cn/apache/hive/
    HIVE_VERSION_LIST=$(wget ${HIVE_SITE} -q -O - | grep -Eo '>hive-[0-9]+\.[0-9]+\.[0-9]+' | grep -Eo '[0-9\.]+')
    echo "当前可用Hive版本号：$(echo $HIVE_VERSION_LIST | sed 's/\s/, /g')。"
    echo "请检查版本号，当前设置的Hadoop版本号:${HADOOP_VERSION}, Hive版本号:${HIVE_VERSION}"
  fi
}

main
