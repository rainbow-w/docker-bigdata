#!/bin/bash

function print_usage() {
  echo "Usage: run VERSION_ID CONTAINER_NAME"
}

if [ $# = 0 ]; then
  print_usage
  exit
fi

VERSION_ID=$1
CONTAINER_NAME=$2

docker build --force-rm -t rainb/bd:$VERSION_ID .

docker run -tid --privileged -v /root/docker:/root -p 8888:8888 -p 11000:11000 -p 11443:11443 -p 9090:9090 -p 8020:8020 -p 8485:8485 -p 50057:50057 -p 50070:50070 -p 50090:50090 -p 8040:8040 -p 8042:8042 -p 8088:8088 -p 19888:19888 -p 50030:50030 -p 50060:50060 --name $CONTAINER_NAME rainb/base:$VERSION_ID  --hostname rainb-bd


docker build --force-rm -t rainb/bd:0.1 .

docker run -ti -v /C/Users/rainb-yss/WorkSpace/Docker/docker-cdh:/root/cdh -p 9090:9090 -p 8020:8020 -p 8485:8485 -p 50057:50057 -p 50070:50070 -p 50090:50090 -p 8040:8040 -p 8042:8042 -p 8088:8088 -p 19888:19888 -p 50030:50030 -p 50060:50060 --hostname rainb-bd --name bd0.1 rainb/bd:0.1


