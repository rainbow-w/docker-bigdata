FROM ubuntu:20.04

LABEL maintainer="rainb <rainbow-w@qq.com>"

ADD install.sh /opt/script

RUN  sed -i "s@http://.*archive.ubuntu.com@http://repo.huaweicloud.com@g" /etc/apt/sources.list \
  && sed -i "s@http://.*security.ubuntu.com@http://repo.huaweicloud.com@g" /etc/apt/sources.list \
  && apt-get update && apt-get install -y \
  && apt install -y \
  curl \
  python3 \
  && rm -rf /var/lib/apt/lists/*

EXPOSE 9870 19888 8088 8188